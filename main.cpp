#include <iostream>

using namespace std;


int main(){
    // Create a program that asks the for the dimensions of a toolshed and generates a
    // PDF quote for the customer

    float width;
    float length;
    float height;

    cin >> width;
    cin >> length;
    cin >> height;

    float wallPricePerSqFt = 5.25;
    float wallArea = width * height * 2 + length * height * 2;
    float wallPrice = wallArea * wallPricePerSqFt;

    float floorPricePerSqFt = 12;
    float floorArea = width * length;
    float floorPrice = floorArea * floorPricePerSqFt;

    float roofPricePerSqFt = 15;
    float roofArea = width * length;
    float roofPrice = roofArea * roofPricePerSqFt;

    float totalPrice = wallPrice + floorPrice + roofPrice;

    cout << "\\documentclass[12pt]{article}" << endl;
    cout << "\\usepackage[margin=1in]{geometry}" << endl;
    cout << "\\title{Tool Shed Quote}" << endl;
    cout << "\\author{}" << endl;
    cout << "\\begin{document}" << endl;
    cout << "\\maketitle" << endl;
    cout << "\\noindent" << endl;
    cout << "\\begin{tabular}{p{5in} r}" << endl;
    cout << "\\hline" << endl;
    cout << "\\textbf{Description} & \\textbf{Price}\\\\" << endl;
    cout << "\\hline" << endl;
    cout << "Wall materials & \\$ " << wallPrice << "\\\\" << endl;
    cout << "Floor materials & \\$ " << floorPrice<< "\\\\" << endl;
    cout << "Roof materials & \\$ " << roofPrice << "\\\\" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\textbf{TOTAL} & \\textbf{\\$ " << totalPrice << "}\\\\" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}" << endl;
    cout << "\\thispagestyle{empty}" << endl;
    cout << "\\end{document}" << endl;

    return 0;
}